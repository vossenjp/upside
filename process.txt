= Development process =

== Tools ==

Contributors should be aware that we have standardized on the
following tools:

* Upverter for eCAD.

* LibreCAD for mechanical design

* asciidoc for documentation

* pic (rendered by dpic to SVG) for box-and-arrow diagrams

* dot (rendered by graphviz to PNG) for general graphs.

Please do not contribute rendered graphs, drawings or schematics to the
repository; they need to be kept in a form suitable for editing,
such as pic markup or dot markup or DXF.

== Procedures ==

We communicate and store information in three different ways: the
project issue tracker, the wiki, and the reopository.

Issues such as bug reports, fearure requests, and arguments about the
design should be initiated on the bugtracker. We use the issue threads
for structured discussion.

The wiki is the project's whiteboard.  Normally when a discussioon
thread resolves, the resolution will go to the wiki. The intent is for
the wiki to represent the parts of the design that are decided vut
not finalized and spelled in detail.

As design decisions turn into implementation plans, those plans can
move to the repository.

== Authority ==

Eric S. Raymond, aka @esr, is the product designer.  It's his job to
maintain the requirements list and prevent dangerous mission creep. He
is also the software lead.

Eric Baskin, aka @NotGump, is the hardware lead and (especially)
our high-power electronics expert.  He is an EE specializing in power
and signals, and has 30 years of experience.

We think our contributors are really smart, and therefore try to make
decisions by consensus among active in discussion of whatever issue is
in play.  Such as decision is usually signaled by one of the Erics
closing an issue; they try not to issue ukases unless they have to.

// end
